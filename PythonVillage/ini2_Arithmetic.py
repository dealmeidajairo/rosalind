# Two positive integers a and b, each less than 1000.

# Return: The integer corresponding to the square of the hypotenuse of the right triangle whose legs have lengths a and b.


a = input("Entre com um lado do triângulo")

b = input("Entre com o outro lado do triângulo")

c = (int(a) ** 2) + (int(b) ** 2)

print ("Resultado - ", c)