#Given: A string s of length at most 200 letters and four integers a, b, c and d
#Return: The slice of this string from indices a through b and c through d (with space in between), inclusively. In other words, we should include elements s[b] and s[d] in our slice.

sequencia = input("Entre com a sequencia ")
qnt = int(input("Quantos trechos quer buscar? "))

busca = ""

for a in range(qnt):
    inicio = int(input("Digite a posição inicial "))
    fim = int(input("Digite a posição final "))
    busca = busca + " " + sequencia[inicio:fim+1]

print(busca)