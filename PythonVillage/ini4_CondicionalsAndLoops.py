#Given: Two positive integers a and b (a<b<10000).
#Return: The sum of all odd integers from a through b, inclusively.

a = int(input("Entre com valor de A "))
b = int(input("Entre com valor de B "))

if a < b:
    c = 0
    while a < b:
        if (a % 2 == 1):
            c = c + a
        a += 1
    print (c)